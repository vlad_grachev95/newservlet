package ru.appline.logic;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Model implements Serializable {
    private static final Model instance = new Model();

    private final Map<Integer, User> model;

    public static Model getInstance() {
        return instance;
    }

    private Model() {
        model = new HashMap<>();

        model.put(1, new User("Vladislav", "Grachyov", 55555));
        model.put(2, new User("Vladimir","Antonov",66666));
        model.put(3, new User("Ivan", "Alekseev",77777));
        model.put(4, new User("Valentin", "Gusev",45632));
    }

    public Map<Integer, User> getFromList() {
        return model;
    }

    public void add(User user, int id) {
        model.put(id, user);
    }

    public void delete(int id) {
        model.remove(id);
    }

    public void put(User userr, int id) {
        add(userr,id);
    }

}
